package es.unex.giis.asee.appdoption;


import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import org.junit.Test;

import java.io.IOException;
import java.util.LinkedList;


import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.Auth;
import es.unex.giis.asee.appdoption.Model.Pet;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RemoteSourceTest {


    @Test
    public void getAuth() throws IOException {
        Auth token = new Auth();
        token.setToken("Test");
        token.setExpiresTime(2000);
        token.setTokenType("Test");

        Gson gson = new Gson();
        String body = gson.toJson(token);

        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(body);
        mockWebServer.enqueue(mockedResponse);


        PetFinderService service = retrofit.create(PetFinderService.class);

        Call<Auth> call = service.getAuth("","","");
        Response<Auth> response = call.execute();
        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        Auth received = response.body();
        assertNotNull(received);
        assertEquals("Test",received.getToken());

        mockWebServer.shutdown();

    }

    @Test
    public void getAnimalsTest() throws IOException {
        Animal animal1 = new Animal();
        animal1.setId(1);
        animal1.setAge("Baby");
        animal1.setName("Test1");

        Animal animal2=new Animal();
        animal2.setId(2);
        animal2.setAge("Senior");
        animal2.setName("Test2");

        Pet petTest = new Pet();


        LinkedList<Animal> animals = new LinkedList<>();
        animals.add(animal1);
        animals.add(animal2);

        petTest.setAnimal(animals);


        ObjectMapper objectMapper = new ObjectMapper();

        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        try {
            mockedResponse.setBody(objectMapper.writeValueAsString(petTest));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        mockWebServer.enqueue(mockedResponse);

        PetFinderService service = retrofit.create(PetFinderService.class);

        Call<Pet> call = service.listPets("",2,"");


        Response<Pet> response = call.execute();

        assertTrue(response != null);
        assertTrue(response.isSuccessful());


        Pet pets =response.body();

        assertFalse(pets.getAnimals().isEmpty());
        assertTrue(pets.getAnimals().size()==2);

        assertTrue(pets.getAnimals().get(0).getAge().equals("Baby"));
        assertTrue(pets.getAnimals().get(1).getAge().equals("Senior"));

        mockWebServer.shutdown();


    }

}
