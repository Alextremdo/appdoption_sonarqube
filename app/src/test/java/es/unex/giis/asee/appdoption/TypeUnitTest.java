package es.unex.giis.asee.appdoption;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.lang.reflect.Field;

public class TypeUnitTest {

    @Test
    public void setHref() throws NoSuchFieldException, IllegalAccessException {
        String value = "href 1";
        es.unex.giis.asee.appdoption.Model.Type instance = new es.unex.giis.asee.appdoption.Model.Type();
        instance.setHref(value);
        final Field field = instance.getClass().getDeclaredField("href");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getHref() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giis.asee.appdoption.Model.Type instance = new es.unex.giis.asee.appdoption.Model.Type();
        final Field field = instance.getClass().getDeclaredField("href");
        field.setAccessible(true);
        field.set(instance, "href 2");

        //when
        final String result = instance.getHref();

        //then
        assertEquals("field wasn't retrieved properly", result, "href 2");
    }
}
