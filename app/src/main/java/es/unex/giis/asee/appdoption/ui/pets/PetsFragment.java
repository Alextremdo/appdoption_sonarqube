package es.unex.giis.asee.appdoption.ui.pets;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.LinkedList;
import java.util.List;

import es.unex.giis.asee.appdoption.AppExecutors;
import es.unex.giis.asee.appdoption.DependencyInjectors.AppContainer;
import es.unex.giis.asee.appdoption.DependencyInjectors.MyApplication;
import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.MyAdapter;
import es.unex.giis.asee.appdoption.Navigate;
import es.unex.giis.asee.appdoption.OnPetsLoadedListener;
import es.unex.giis.asee.appdoption.PetsDiskLoaderRunnable;
import es.unex.giis.asee.appdoption.R;
import es.unex.giis.asee.appdoption.ViewModels.MyPetsViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PetsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PetsFragment extends Fragment implements OnPetsLoadedListener{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View mView;
    private MyAdapter mAdapter;
    private RecyclerView recyclerView;
    private Context context;
    public PetsFragment() {
        // Required empty public constructor
    }

    public void onAttach(Context context){
        super.onAttach(context);
        this.context=context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PetsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PetsFragment newInstance(String param1, String param2) {
        PetsFragment fragment = new PetsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_pets, container, false);

        recyclerView = mView.findViewById(R.id.petList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));

        mAdapter=new MyAdapter(new LinkedList<Animal>(),(MyAdapter.OnListIterationListener)getContext(),"mypets");
        recyclerView.setAdapter(mAdapter);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;

        MyPetsViewModel viewModel = new ViewModelProvider(this,appContainer.userPetsFactory).get(MyPetsViewModel.class);
        viewModel.getUserPets().observe(getActivity(),this::onPetsLoaded);

        TextView tViewPet = mView.findViewById(R.id.mypetsttitle);

        FloatingActionButton fab = (FloatingActionButton) mView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavDirections action = PetsFragmentDirections.actionNavMypetsToCreatePet2();
                ((Navigate)context).navigateTo(action);
            }
        });


        return mView;
    }

    @Override
    public void onPetsLoaded(List<Animal> pets) {
        mAdapter.swap(pets);
    }
}