package es.unex.giis.asee.appdoption.Factories;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giis.asee.appdoption.ViewModels.HomeFragmentViewModel;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;


/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link PetsRepository}
 * Debe de heredar de ViewModelProvider
 */
public class HomeViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final PetsRepository mRepository;

    public HomeViewModelFactory(PetsRepository repository) {
        this.mRepository = repository;
    }


    /**
     * Debe de sobrescribir el metodo create para así crear el ViewModel concreto.
     * @param modelClass
     * @param <T> Clase a generar, debe de heredar de ViewModel
     * @return ViewModel generado
     */
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new HomeFragmentViewModel(mRepository);
    }
}