package es.unex.giis.asee.appdoption.ui.incidence;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import es.unex.giis.asee.appdoption.R;
import es.unex.giis.asee.appdoption.Navigate;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link IncidenceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IncidenceFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;
    private View mView;
    private Context context;

    public IncidenceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context=context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment IncidenceFragment.
     */
    public static IncidenceFragment newInstance(String param1, String param2) {
        IncidenceFragment fragment = new IncidenceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_incidence, container, false);

        Button btn = (Button)mView.findViewById(R.id.send_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = "AppDoptionIncidence@appdoption.es";
                String content = ((TextView)mView.findViewById(R.id.issuedescription)).getText().toString();
                Intent intent = new Intent (Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Incidence with app");
                intent.putExtra(Intent.EXTRA_TEXT, content);
                intent.setPackage("com.google.android.gm");
                if (intent.resolveActivity(context.getPackageManager())!=null)
                    startActivity(intent);
                else
                    Toast.makeText(getContext(),"Gmail App is not installed", Toast.LENGTH_SHORT).show();
            }
        });

        Button cnclBtn = (Button) mView.findViewById(R.id.cancel_btn);
        cnclBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavDirections action = IncidenceFragmentDirections.actionNavIncidenceToNavHome();
                ((Navigate)context).navigateTo(action);
            }
        });


        return mView;
    }
}