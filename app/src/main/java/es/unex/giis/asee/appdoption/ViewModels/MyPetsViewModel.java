package es.unex.giis.asee.appdoption.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;

public class MyPetsViewModel extends ViewModel {
    private final PetsRepository mRepository;
    private final LiveData<List<Animal>> mUserPets;


    public MyPetsViewModel(PetsRepository mRepository) {
        this.mRepository = mRepository;
        this.mUserPets=mRepository.getUserPets();
    }

    public LiveData<List<Animal>> getUserPets(){
        return mUserPets;
    }

}
