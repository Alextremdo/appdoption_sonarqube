package es.unex.giis.asee.appdoption;

import androidx.navigation.NavDirections;

public interface Navigate {
    public void navigateTo(NavDirections action);
}
