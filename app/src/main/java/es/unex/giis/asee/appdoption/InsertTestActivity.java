package es.unex.giis.asee.appdoption;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import es.unex.giis.asee.appdoption.ui.crud_create.CRUDCreatePetFragment;

public class InsertTestActivity extends AppCompatActivity {
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_insert_test);
            CRUDCreatePetFragment fragment = new CRUDCreatePetFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.insertTest, fragment).commit();
        }
}