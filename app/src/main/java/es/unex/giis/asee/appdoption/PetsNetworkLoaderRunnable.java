package es.unex.giis.asee.appdoption;


import android.util.Log;

import java.io.IOException;

import es.unex.giis.asee.appdoption.Model.Pet;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PetsNetworkLoaderRunnable implements Runnable{
    private final OnPetsLoadedListener mOnPetsLoadedListener;

    public PetsNetworkLoaderRunnable(OnPetsLoadedListener onPetsLoadedListener) {
        mOnPetsLoadedListener = onPetsLoadedListener;
    }

    @Override public void run() {
        // Instanciación de Retrofit y llamada síncrona
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.petfinder.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PetFinderService service = retrofit.create(PetFinderService.class);
        try {
            String token=service.getAuth("client_credentials","5RFNCQA88VsG47MxsUFEFGeLCGrlUIujvOwK9UZzrnTIOgxqwT","mXCKp3qBHYgPTcNQA4BC5SOemp3BNBOjfaWD6nHw").execute().body().getToken();


            Pet pets = service.listPets(" Bearer " + token,20, "adoptable").execute().body();


           AppExecutors.getInstance().mainThread().execute(new Runnable() {
                @Override
                public void run() {
                    mOnPetsLoadedListener.onPetsLoaded(pets.getAnimals());
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
