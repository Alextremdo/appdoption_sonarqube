package es.unex.giis.asee.appdoption;

import android.content.Context;

import java.util.LinkedList;
import java.util.List;

import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.Favorite;
import es.unex.giis.asee.appdoption.Model.User;
import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;

public class FavouritesPetsDiskLoaderRunnable implements Runnable{
    private final OnPetsLoadedListener mOnPetsLoadedListener;
    private Context context;


    public FavouritesPetsDiskLoaderRunnable(OnPetsLoadedListener mOnPetsLoadedListener, Context context) {
        this.mOnPetsLoadedListener = mOnPetsLoadedListener;
        this.context=context;
    }


    @Override
    public void run() {
        List<Favorite> favs=null;
        AppDoptionDatabase db = AppDoptionDatabase.getInstace(context);
        User user = db.getUserDAO().getUser();
        List<Animal> finalAnimalsDB = new LinkedList<>();
        for (Favorite f : favs){
            finalAnimalsDB.add(f.getAnimalfav());
        }
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                mOnPetsLoadedListener.onPetsLoaded(finalAnimalsDB);
            }
        });
    }
}
