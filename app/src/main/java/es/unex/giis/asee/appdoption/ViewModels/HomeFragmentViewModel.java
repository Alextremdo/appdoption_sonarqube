package es.unex.giis.asee.appdoption.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.datamanagement.PetsRepository;


public class HomeFragmentViewModel extends ViewModel {
    private final PetsRepository mRepository;
    private final LiveData<List<Animal>> mHomePets;


    public HomeFragmentViewModel(PetsRepository mRepository) {
        this.mRepository = mRepository;
        this.mHomePets = mRepository.getHomePets();
    }

    public LiveData<List<Animal>> getHomePets(){
        return mRepository.getHomePets();
    }


}
