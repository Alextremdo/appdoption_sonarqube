package es.unex.giis.asee.appdoption;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import es.unex.giis.asee.appdoption.ui.pet.PetFragment;

public class DeleteFavouriteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_favourite);
        PetFragment fragment = new PetFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.deleteFavouriteTest, fragment).commit();
    }
}