package es.unex.giis.asee.appdoption;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import es.unex.giis.asee.appdoption.ui.contact.ContactFragment;

public class CancelAdoptionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_adoption);
        ContactFragment contactFragment = new ContactFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.cancelAct,contactFragment)
                .commit();
    }
}