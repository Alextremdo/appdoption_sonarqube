
package es.unex.giis.asee.appdoption.Model;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

@Entity
public class Animal implements Serializable
{

    @SerializedName("id")
    @Expose
    @PrimaryKey
    private Integer id;
    @SerializedName("organization_id")
    @Expose
    private String organizationId;
    private String photo;
    @SerializedName("url")
    @Expose
    @Ignore
    private String url;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("species")
    @Expose
    private String species;
    @SerializedName("breeds")
    @Expose
    @Embedded
    private Breeds breeds;
    @SerializedName("colors")
    @Expose
    @Ignore
    private Colors colors;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("coat")
    @Expose
    @Ignore
    private String coat;
    @SerializedName("attributes")
    @Expose
    @Ignore
    private Attributes attributes;
    @SerializedName("environment")
    @Expose
    @Ignore
    private Environment environment;
    @SerializedName("tags")
    @Expose
    @Ignore
    private List<Object> tags = null;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("organization_animal_id")
    @Expose
    private String organizationAnimalId;
    @SerializedName("photos")
    @Expose
    @Ignore
    private List<Photo> photos = null;
    @SerializedName("primary_photo_cropped")
    @Expose
    @Ignore
    private PrimaryPhotoCropped primaryPhotoCropped;
    @SerializedName("videos")
    @Expose
    @Ignore
    private List<Object> videos = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_changed_at")
    @Expose
    private String statusChangedAt;
    @SerializedName("published_at")
    @Expose
    private String publishedAt;
    @SerializedName("distance")
    @Expose
    @Ignore
    private Object distance;
    @SerializedName("contact")
    @Expose
    @Embedded
    private Contact contact;
    @SerializedName("_links")
    @Expose
    @Ignore
    private Links links;
    @Ignore
    private final static long serialVersionUID = 8957871103197621626L;

    @Ignore
    public Animal(){

    }

    public Animal(Integer id, String organizationId, String type, String species, String age, String gender, String size, String name, String description, String organizationAnimalId, String status, String statusChangedAt, String publishedAt, Contact contact, String photo) {
        this.id = id;
        this.organizationId = organizationId;
        this.type = type;
        this.species = species;
        this.age = age;
        this.gender = gender;
        this.size = size;
        this.name = name;
        this.description = description;
        this.organizationAnimalId = organizationAnimalId;
        this.status = status;
        this.statusChangedAt = statusChangedAt;
        this.publishedAt = publishedAt;
        this.contact=contact;
        this.photo=photo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public Breeds getBreeds() {
        return breeds;
    }

    public void setBreeds(Breeds breeds) {
        this.breeds = breeds;
    }

    public Colors getColors() {
        return colors;
    }

    public void setColors(Colors colors) {
        this.colors = colors;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCoat() {
        return coat;
    }

    public void setCoat(String coat) {
        this.coat = coat;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public List<Object> getTags() {
        return tags;
    }

    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganizationAnimalId() {
        return organizationAnimalId;
    }

    public void setOrganizationAnimalId(String organizationAnimalId) {
        this.organizationAnimalId = organizationAnimalId;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public PrimaryPhotoCropped getPrimaryPhotoCropped() {
        return primaryPhotoCropped;
    }

    public void setPrimaryPhotoCropped(PrimaryPhotoCropped primaryPhotoCropped) {
        this.primaryPhotoCropped = primaryPhotoCropped;
    }

    public List<Object> getVideos() {
        return videos;
    }

    public void setVideos(List<Object> videos) {
        this.videos = videos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusChangedAt() {
        return statusChangedAt;
    }

    public void setStatusChangedAt(String statusChangedAt) {
        this.statusChangedAt = statusChangedAt;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public Object getDistance() {
        return distance;
    }

    public void setDistance(Object distance) {
        this.distance = distance;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

}
