package es.unex.giis.asee.appdoption.ui.crud_create;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import es.unex.giis.asee.appdoption.AppExecutors;
import es.unex.giis.asee.appdoption.DependencyInjectors.AppContainer;
import es.unex.giis.asee.appdoption.DependencyInjectors.MyApplication;
import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.Breeds;
import es.unex.giis.asee.appdoption.Model.Contact;
import es.unex.giis.asee.appdoption.Model.User;
import es.unex.giis.asee.appdoption.Navigate;
import es.unex.giis.asee.appdoption.R;
import es.unex.giis.asee.appdoption.ViewModels.CreatePetViewModel;
import es.unex.giis.asee.appdoption.ViewModels.UserFragmentViewModel;
import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CRUDCreatePetFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CRUDCreatePetFragment extends Fragment {

    private View mview;
    private Context context;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;
    private Animal animal;

    public CRUDCreatePetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context=context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreatePetFragment.
     */

    public static CRUDCreatePetFragment newInstance(String param1, String param2) {
        CRUDCreatePetFragment fragment = new CRUDCreatePetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview = inflater.inflate(R.layout.fragment_crud_createpet, container, false);

        Button cnclBtn = (Button) mview.findViewById(R.id.cancel_btn);
        cnclBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavDirections action = CRUDCreatePetFragmentDirections.actionCreatePetToNavHome();
                ((Navigate)context).navigateTo(action);
            }
        });

        AppContainer appContainer = ((MyApplication)getActivity().getApplication()).appContainer;
        CreatePetViewModel cpetVModel = new ViewModelProvider(this,appContainer.createPetFactory).get(CreatePetViewModel.class);
        UserFragmentViewModel userVModel = new ViewModelProvider(this,appContainer.userFactory).get(UserFragmentViewModel.class);
        Button createBtn = (Button) mview.findViewById(R.id.send_btn);
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        animal=new Animal();
                        /*List<Photo> photos=new LinkedList<>();
                        Photo p = new Photo();
                        p.setFull(getImage().toString());
                        photos.add(p);
                        animal.setPhotos(photos);*/
                        animal.setPhotos(null);
                        animal.setName(((TextView)mview.findViewById(R.id.animal_name)).getText().toString());
                        Breeds breed = new Breeds();
                        breed.setPrimary(((TextView)mview.findViewById(R.id.animal_breed)).getText().toString());
                        animal.setBreeds(breed);
                        animal.setAge(((TextView)mview.findViewById(R.id.animal_age)).getText().toString());

                        User user = userVModel.getUser();
                        Contact contact = new Contact(user.getEmail(),user.getTelephone(),user.getAddress());
                        animal.setContact(contact);
                        animal.setStatus("Adoptable");
                        animal.setDescription(((TextView)mview.findViewById(R.id.animal_description)).getText().toString());
                        cpetVModel.insertAnimalDB(animal);
                    }
                });
                NavDirections action = CRUDCreatePetFragmentDirections.actionCreatePetToNavMypets();
                ((Navigate)context).navigateTo(action);
            }
        });

        return mview;
    }
}