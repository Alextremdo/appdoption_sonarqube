package es.unex.giis.asee.appdoption;

import java.util.List;

import es.unex.giis.asee.appdoption.Model.Favorite;


//NECESARIA PARA CARGAR FAVORITOS
public interface OnFavsLoaded {
    public void onFavsLoaded(List<Favorite> favs);
}
