package es.unex.giis.asee.appdoption.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Auth implements Serializable {

    @SerializedName("token_type")
    @Expose
    private String tokenType;

    @SerializedName("expire_time")
    @Expose
    private int expiresTime;

    @SerializedName("access_token")
    @Expose
    public String accessToken;

    public Auth(){
    }

    public String getToken(){
        return accessToken;
    }

    public void setToken(String token){accessToken=token;}
    public void setExpiresTime(int expiresTime){this.expiresTime=expiresTime;}
    public void setTokenType(String tokenType){this.tokenType=tokenType;}

}
