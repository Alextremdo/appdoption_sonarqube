package es.unex.giis.asee.appdoption.roomdb;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;


import androidx.room.Database;
import androidx.room.Room;

import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.Favorite;
import es.unex.giis.asee.appdoption.Model.User;

@Database(entities = {Animal.class, User.class, Favorite.class}, version=1)
public abstract class AppDoptionDatabase extends RoomDatabase {
    //Se implementa el patron Singleton ya que solo se desea que se instancie una vez
    public static AppDoptionDatabase instace;

    //El contexto es necesario ya que Room hace uso del mismo para generar la BD
    public static AppDoptionDatabase getInstace(Context context) {
        if(instace==null){
            instace = Room.databaseBuilder(context,AppDoptionDatabase.class,"appdoption.db").build();
        }
        return instace;
    }
    //Room implementara este metodo en tiempo de compilacion
    public abstract AnimalDAO getAnimalDAO();

    public abstract UserDAO getUserDAO();

    public abstract FavoriteDAO getFavDAO();

}
